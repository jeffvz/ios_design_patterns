protocol Student {
    var skill : String {get set}
    var practiceTime : Double {get set}
    
    func practiceTheSkill()
    func improveTheSkill()
}
class Guitarist:Student{
    var practiceTime: Double
    
    var skill: String
    
    
    init(skill:String) {
        self.skill = skill
        self.practiceTime = 2.0
    }
    
    func practiceTheSkill() {
        print("I'm practicing my guitar skills")
    }
    
    func improveTheSkill() {
        print("I'm going to improve my skill by practicing for: ")
        print(practiceTime*2)
    }
}

class Athlete:Student{
    var skill: String
    var practiceTime: Double
    
    init(skill:String) {
        self.skill = skill
        self.practiceTime = 1.5
    }
    
    func practiceTheSkill() {
        print("I'm practicing my athletics skills")
    }
    
    func improveTheSkill() {
        print("I'm going to improve my skill by practicing for: ")
        print(practiceTime*3)
    }
}


class StudentFactory{
    
    func makeAStudent(skill:String) -> Student{
        switch skill {
        case "A":
            return Athlete(skill: skill)
        case "G":
            return Guitarist(skill: skill)
        default:
            return Guitarist(skill: skill)
        }
    }
    
}

let guitaristStudent = StudentFactory().makeAStudent(skill: "G")
guitaristStudent.practiceTheSkill()
guitaristStudent.improveTheSkill()