class Database {
    static var databaseInstance:Database  = {
        let instance = Database()
        return instance
    }()
    
    private init() {
        print("Database connection")
    }
    
    func querySomething() -> String {
        return "Here goes some query to database!"
    }
}

let databaseInstance:Database = Database.databaseInstance
databaseInstance.querySomething()
