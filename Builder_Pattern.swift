protocol Builder
{
    func buildComputerMainboard()
    func buildComputerProcessor()
    func buildComputerRAM()
    func buildComputerDisk()
}

class Computer
{
    private var parts = [String]()

    func assembly(part: String) {
        self.parts.append(part)
    }

    func assembledComputer() -> String {
        return "Computer: " + parts.joined(separator: " - ") + "\n"
    }
}

class ComputerBuilder:Builder
{
    private var computer = Computer()
    
    func buildComputerMainboard() {
        computer.assembly(part: "Asus")
    }
    
    func buildComputerProcessor() {
        computer.assembly(part: "i9 9550u")
    }
    
    func buildComputerRAM() {
        computer.assembly(part: "32 GB")
    }
    
    func buildComputerDisk() {
        computer.assembly(part: "1 TB SSD")
    }
    
    func reset() {
        computer = Computer()
    }
    
    func getComputer() -> Computer{
        let computer = self.computer
        reset()
        return computer
    }
}

class ComputerAssemblyDirector
{
    private var computerBuilder : ComputerBuilder
    
    init(computerBuilder : ComputerBuilder) {
        self.computerBuilder = computerBuilder
    }
    
    func getComputer() -> Computer{
        return self.computerBuilder.getComputer()
    }
    
    func makeAComputer(){
        self.computerBuilder.buildComputerMainboard()
        self.computerBuilder.buildComputerProcessor()
        self.computerBuilder.buildComputerRAM()
        self.computerBuilder.buildComputerDisk()
    }
    
    func makeAComputerWithoutDisk(){
        self.computerBuilder.buildComputerMainboard()
        self.computerBuilder.buildComputerProcessor()
        self.computerBuilder.buildComputerRAM()
    }
}

let computerBuilder = ComputerBuilder()
let computerDirector = ComputerAssemblyDirector(computerBuilder: computerBuilder)

computerDirector.makeAComputer()
let firstComputer = computerDirector.getComputer()

computerDirector.makeAComputerWithoutDisk()
let computerWithoutDisk = computerDirector.getComputer()

print(firstComputer.assembledComputer())
print(computerWithoutDisk.assembledComputer())

/*
 prints:
 
    Computer: Asus - i9 9550u - 32 GB - 1 TB SSD

    Computer: Asus - i9 9550u - 32 GB
 */