import UIKit

protocol PizzaBase{
    func getPrice() -> Double
    func getName() -> String
}

class PizzaGourmet: PizzaBase {
    private var gourmetPrice = 2.90
    private var name = "Gourmet"
    
    func getPrice() -> Double {
        return self.gourmetPrice
    }
    
    func getName() -> String {
        return "Pizza "+self.name
    }
    
}

class PizzaDecorator: PizzaBase {
    private var decoratedSource: PizzaBase
    
    init(pizzaDecorator:PizzaBase){
        self.decoratedSource = pizzaDecorator
    }
    func getPrice() -> Double {
        return self.decoratedSource.getPrice()
    }
    
    func getName() -> String {
        return self.decoratedSource.getName()
    }

}

class ExtraCheese: PizzaDecorator{
    private var decoratedSource: PizzaBase
    
    override init(pizzaDecorator:PizzaBase){
        self.decoratedSource = pizzaDecorator
        super.init(pizzaDecorator: pizzaDecorator)
    }
    override func getPrice() -> Double {
        return self.decoratedSource.getPrice()+0.50
    }
    
    override func getName() -> String {
        return self.decoratedSource.getName()+" with extra cheese"
    }
}


var pizzaWithExtraChesse:PizzaBase = ExtraCheese(pizzaDecorator: PizzaGourmet())
print(pizzaWithExtraChesse.getName())
print(pizzaWithExtraChesse.getPrice())

/*
 PRINTS
* Pizza Gourmet with extra cheese
* 3.4

 */
