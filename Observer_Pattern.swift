import UIKit

protocol ConsoleConnector: class {
    func update(section:Int, sectionName:String, state:Bool)
}

class MixingConsole{
    private var connectors: Array<ConsoleConnector> = Array()
    
    func plug(connector:ConsoleConnector){
        self.connectors.append(connector)
        print("New device plugged")
    }
    
    func unplug(connector:ConsoleConnector){
        if let indexConnector = self.connectors.firstIndex(where: {$0 === connector}){
            self.connectors.remove(at: indexConnector)
            print("New device plugged")
        }
    }
    
    func setUpConsole(section:Int, sectionName:String, state:Bool){
        for connector in self.connectors{
            connector.update(section: section, sectionName: sectionName, state:
                state)
        }
    }
}

class Microphone:ConsoleConnector{
    func update(section: Int, sectionName: String, state: Bool) {
        print("Microphones have been plugged in section: \(section) -> \(sectionName)")
    }
}

class Screen:ConsoleConnector{
    func update(section: Int, sectionName: String, state: Bool) {
        print("Screen have been plugged in section: \(section) -> \(sectionName)")
    }
}

/*Run */
let mixingConsole = MixingConsole()

let microphoneInstance = Microphone()
let screenInstance = Screen()

mixingConsole.plug(connector: microphoneInstance)
mixingConsole.plug(connector: screenInstance)

mixingConsole.setUpConsole(section: 1, sectionName: "Section Microphones-Screens", state: true)

mixingConsole.unplug(connector: microphoneInstance)

/*
 PRINTS:
     New device plugged
     New device plugged
     Microphones have been plugged in section: 1 -> Section Microphones-Screens
     Screen have been plugged in section: 1 -> Section Microphones-Screens
     New device plugged
 */
