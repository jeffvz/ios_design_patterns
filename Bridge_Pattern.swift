protocol Guitar{
    func isOff() -> Bool
    func powerOn() -> String
    func powerOff() -> String
    func getVolume() -> Int
    func setVolume(volume:Int)
    func setEffect(efect:String)
}

class GuitarPedal
{
    private var guitar:Guitar
    
    init(guitar:Guitar){
        self.guitar = guitar
    }
    
    func togglePower() -> String{
        if self.guitar.isOff(){
            return self.guitar.powerOn()
        }
        else{
            return self.guitar.powerOff()
        }
    }
    
    func volumeDown() {
        self.guitar.setVolume(volume: (self.guitar.getVolume() - 5))
    }
    func volumeUp(){
        self.guitar.setVolume(volume: (self.guitar.getVolume() + 5))
    }
    
    func switchEffect(efect:String){
        self.guitar.setEffect(efect:efect)
    }
}

class ElectricGuitar:Guitar
{
    var volume:Int = 0
    var effect:String = ""
    
    func isOff() -> Bool {
        return true
    }
    
    func powerOn() -> String {
        return "Turned on"
    }
    
    func powerOff() -> String {
        return "Turned off"
    }
    
    func getVolume() -> Int {
        return self.volume
    }
    
    func setVolume(volume: Int) {
        return self.volume = volume
    }
    
    func setEffect(efect: String) {
        return self.effect = efect
    }
}



class ClassicGuitar:Guitar
{
    var volume:Int = 5
    var effect:String = "Clean"
    
    func isOff() -> Bool {
        return false
    }
    
    func powerOn() -> String {
        return "Turned on"
    }
    
    func powerOff() -> String {
        return "Turned off"
    }
    
    func getVolume() -> Int {
        return self.volume
    }
    
    func setVolume(volume: Int) {
        return self.volume = volume
    }
    
    func setEffect(efect: String) {
        print("I'm a classic guitar, my effect is: "+self.effect)
    }
}

let electricGuitar = ElectricGuitar()
var guitarPedal = GuitarPedal(guitar:electricGuitar)

print(guitarPedal.togglePower())
guitarPedal.switchEffect(efect: "Distortion")

guitarPedal.volumeUp()
guitarPedal.volumeUp()

print(electricGuitar.effect)
print(electricGuitar.volume)

/*
 prints:
    Turned on
    Distortion
    10
 */
